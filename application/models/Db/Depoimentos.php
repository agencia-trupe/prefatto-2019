<?php

class Application_Model_Db_Depoimentos extends ZendPlugin_Db_Table 
{
    protected $_name = "depoimentos";
    protected $_foto_join_table = 'depoimentos_fotos'; // tabela de relação para registros de fotos
    protected $_foto_join_table_field = 'depoimento_id'; // campo de relação para registros da tabela principal
    protected $_foto_join_field = 'foto_id'; // campo para dar join com foto.id
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
    /**
     * Retorna as fotos da promoção
     *
     * @param int  $id    - id da promoção
     * @param bool $check - checar se promoção existe?
     *
     * @return array - rowset com fotos da promoção
     */
    public function getFotos1($id,$check=true)
    {
        $depoimento = $this->fetchRow('id="'.$id.'"');

        if($check && !$depoimento) return false;
        
        $fotos = array();
        
        if($depoimento_fotos = $depoimento->findDependentRowset('Application_Model_Db_DepoimentosFotos')){
            foreach($depoimento_fotos as $depoimento_foto){
                $fotos[] = Is_Array::utf8DbRow($depoimento_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    /**
     * Retorna fotos dos depoimentos
     * 
     * @param array $depoimentos - rowset de depoimentos para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - depoimentos com fotos
     */
    public function getFotos($depoimentos,$withObjects=false)
    {
        $pids = array(); // ids de depoimentos

        // identificando depoimentos
        foreach($depoimentos as $produto) $pids[] = $produto->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('depoimentos_fotos as fc',array('fc.depoimento_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.depoimento_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            $fotos = $select->query()->fetchAll();
            $fotos = array_map('Is_Array::toObject',$fotos);
        } else {
            $bDepoimentos_fotos = new Application_Model_Db_DepoimentosFotos();
            $fotos = $bDepoimentos_fotos->fetchAll('depoimento_id in ('.implode(',',$pids).')');
        }

        // associando fotos
        foreach($depoimentos as &$produto){
            $produto->fotos = $this->getFotosSearch($produto->id,$fotos);
        }

        return $depoimentos;
    }

    /**
     * Monta rowset de fotos com base no produto_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->depoimento_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }

    public function getRandom($limit=null)
    {
        return Is_Array::utf8DbResult(
            $this->fetchAll('status_id = 1', new Zend_Db_Expr('RAND()'), $limit)
        );
    }
    
}