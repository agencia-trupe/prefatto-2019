<?php

class Application_Model_Db_Arquivos extends Zend_Db_Table {
    protected $_name = "arquivos";

    public static $tipos = array(
        1 => 'Newsletter',
        2 => 'Artigos',
        3 => 'Publicações',
    );
    public static $areas = array(
        1 => 'Newsletter',
        2 => 'Artigos',
        3 => 'Publicações',
    );
    
    protected $_dependentTables = array('Application_Model_Db_PaginasArquivos','Application_Model_Db_ProdutosItemsArquivos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_PaginasArquivos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PaginasArquivos',
            'refColumns'    => 'arquivo_id'
        ),
        'Application_Model_Db_ProdutosItemsArquivos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutosItemsArquivos',
            'refColumns'    => 'arquivo_id'
        ),
    );

    public static function area($id=null) { return $id ? @self::$areas[$id] : self::$areas; }
    public function getArea($id=null) { return self::areas($id); }
    
    public static function tipo($id=null) { return $id ? @self::$tipos[$id] : self::$tipos; }
    public function getTipo($id=null) { return self::tipos($id); }
}