<?php

class Application_Model_Db_Servicos extends ZendPlugin_Db_Table 
{
    protected $_name = "servicos";
    protected $_foto_join_table = 'servicos_fotos';
    protected $_foto_join_table_field = 'servico_id';
    
    /**
     * Retorna registro por alias
     * 
     * @param array $rows - rowset para buscar, caso não seja passado busca na tabela
     */
    public function getByAlias($alias,$rows=null)
    {
        if(!$rows) return $this->fetchRow('alias = "'.$alias.'"');

        $find = null;
        
        foreach($rows as $row) if($row->alias == $alias) $find = $row;

        return $find;
    }

    /**
     * Retorna as fotos do da servico
     *
     * @param int $id - id da servico
     *
     * @return array - rowset com fotos da servico
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('servicos_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('servico_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos servicos
     * 
     * @param array $servicos - rowset de servicos para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - servicos com fotos
     */
    public function getFotos($servicos,$withObjects=false)
    {
        $pids = array(); // ids de servicos

        // identificando servicos
        foreach($servicos as $servico) $pids[] = $servico->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('servicos_fotos as fc',array('fc.servico_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.servico_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $eServicos_fotos = new Application_Model_Db_ServicosFotos();
            $fotos = $eServicos_fotos->fetchAll('servico_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($servicos as &$servico){
            $servico->fotos = $this->getFotosSearch($servico->id,$fotos);
        }

        return $servicos;
    }

    /**
     * Monta rowset de fotos com base no servico_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->servico_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}