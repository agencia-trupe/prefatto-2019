<?php

class Application_Model_Db_ProdutosFotos2 extends Zend_Db_Table
{
    protected $_name = "produtos_fotos2";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Produtos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Produtos' => array(
            'columns' => 'produto_id',
            'refTableClass' => 'Application_Model_Db_Produtos',
            'refColumns'    => 'id'
        )
    );
}
