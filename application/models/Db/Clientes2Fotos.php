<?php

class Application_Model_Db_Clientes2Fotos extends Zend_Db_Table
{
    protected $_name = "clientes2_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Clientes2');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Clientes2' => array(
            'columns' => 'cliente_id',
            'refTableClass' => 'Application_Model_Db_Clientes2',
            'refColumns'    => 'id'
        )
    );
}
