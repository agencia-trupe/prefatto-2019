<?php

class Application_Model_Db_Faqs extends ZendPlugin_Db_Table 
{
    protected $_name = "faqs";
    protected $_foto_join_table = 'faqs_fotos';
    protected $_foto_join_table_field = 'faq_id';
    
    /**
     * Retorna registro por alias
     * 
     * @param array $rows - rowset para buscar, caso não seja passado busca na tabela
     */
    public function getByAlias($alias,$rows=null)
    {
        if(!$rows) return $this->fetchRow('alias = "'.$alias.'"');

        $find = null;
        
        foreach($rows as $row) if($row->alias == $alias) $find = $row;

        return $find;
    }

    /**
     * Retorna as fotos do da faq
     *
     * @param int $id - id da faq
     *
     * @return array - rowset com fotos da faq
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('faqs_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('faq_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos faqs
     * 
     * @param array $faqs - rowset de faqs para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - faqs com fotos
     */
    public function getFotos($faqs,$withObjects=false)
    {
        $pids = array(); // ids de faqs

        // identificando faqs
        foreach($faqs as $faq) $pids[] = $faq->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('faqs_fotos as fc',array('fc.faq_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.faq_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $eFaqs_fotos = new Application_Model_Db_FaqsFotos();
            $fotos = $eFaqs_fotos->fetchAll('faq_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($faqs as &$faq){
            $faq->fotos = $this->getFotosSearch($faq->id,$fotos);
        }

        return $faqs;
    }

    /**
     * Monta rowset de fotos com base no faq_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->faq_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}