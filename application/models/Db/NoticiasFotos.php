<?php

class Application_Model_Db_NoticiasFotos extends Zend_Db_Table
{
    protected $_name = "noticias_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Noticias');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Noticias' => array(
            'columns' => 'noticia_id',
            'refTableClass' => 'Application_Model_Db_Noticias',
            'refColumns'    => 'id'
        )
    );
}
