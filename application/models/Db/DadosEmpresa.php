<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_DadosEmpresa extends ZendPlugin_Db_Table 
{
    protected $_name = "dados_empresa";

    public function htmlTels($row)
    {
        $html = Is_Format::tel($row->tel1,'tel','-');
        if((bool)$row->tel2) $html.= ' | '.Is_format::tel($row->tel2,'tel','-');
        if((bool)$row->tel3) $html.= '<br>'.Is_format::tel($row->tel3,'tel','-');
        if((bool)$row->tel4) $html.= ' | '.Is_format::tel($row->tel4,'tel','-');
        $html.= '<br>';

        return $html;
    }
}