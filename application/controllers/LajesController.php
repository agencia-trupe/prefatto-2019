<?php

class LajesController extends Zend_Controller_Action
{
	public $categs = null;
	public $aplics = null;
	public $prods  = null;

    public function init()
    {
        $this->produtos = db_table('produtos');
        $this->categorias = db_table('categorias');
        $this->aplicacoes = db_table('aplicacoes');

        $this->view->titulo2 = 'Aqui todos os tipos de lajes pré-fabricadas em um mesmo canal de vendas!';
    }

    public function indexAction()
    {
        $this->loadBusca();
        $this->loadBuscaAplicacao();
    }

    public function aplicacaoAction()
    {
        $this->indexAction();

        $alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
        if(!$alias) return;
        $alias = explode('-', $alias);
        $id = array_pop($alias);
        $alias = implode('-', $alias);
        // _d(array($id,$alias));

        $apl = @$this->view->busca_aplicacoes[$id];
        if(!$apl) return $this->_redirect('lajes/aplicacao');
        if($apl->alias!=$alias) return $this->_redirect('lajes/aplicacao');
        // _d($apl);

        $this->view->busca_aplicacao_active = $apl;
    }

    public function loadBusca()
    {
    	$this->getCategs();
    	$this->getProds();
    	$categorias = array();
        foreach($this->categs as $cat) {
        	$categorias[$cat->id] = $cat;
        	$categorias[$cat->id]->produtos = array();
        }
        foreach($this->prods  as $pro) 
        	$categorias[$pro->categoria_id]->produtos[] = $pro;
        // _d($categorias);

        $this->view->busca_categorias = $categorias;
        return $categorias;
    }

    public function loadBuscaAplicacao()
    {
        $this->getAplics();
        $this->getProds();
        $aplicacoes = array();
        foreach($this->aplics as $apl) {
        	$aplicacoes[$apl->id] = $apl;
        	$aplicacoes[$apl->id]->produtos = array();
        }
        foreach($this->prods  as $pro) {
        	$apls = explode(',',$pro->aplicacoes);
        	array_pop($apls); array_shift($apls);
        	foreach($apls as $aplid) $aplicacoes[$aplid]->produtos[] = $pro;
        }
        // _d($aplicacoes);

        $this->view->busca_aplicacoes = $aplicacoes;
        return $aplicacoes;
    }

    public function getCategs()
    {
    	if($this->categs) return;
    	// _d('getCategs',0);
		$this->categs = $this->categorias->fetchAllWithPhoto(
        	'status_id=1',
        	'ordem'
        );
    }
    public function getAplics()
    {
    	if($this->aplics) return;
    	// _d('getAplics',0);
    	$this->aplics = _utfRows($this->aplicacoes->fetchAll(
        	'status_id=1',
        	'ordem'
        ));
    }
    public function getProds()
    {
    	if($this->prods) return;
    	// _d('getProds',0);
        $this->prods = $this->produtos->fetchAllWithPhoto(
        	'status_id=1 and categoria_id is not null',
        	'categoria_id, titulo',
        	null, null, array(
                'group' => 't1.id'
            )
        );
    }


}

