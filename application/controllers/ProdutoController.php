<?php

class ProdutoController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->produtos = db_table('produtos');
        $this->categorias = db_table('categorias');
        $this->view->produtos = $this->produtos;

        require_once APP_PATH.'/controllers/LajesController.php';
        $lajesCtl = new LajesController($this->_request,$this->_response);
        $lajesCtl->indexAction();
    }

    public function indexAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	if(!$alias) return $this->_redirect('lajes');
    	$alias = explode('-', $alias);
    	$id = (int)array_pop($alias);
    	$alias = implode('-', $alias);
    	// _d(array($id,$alias));

    	$row = _utfRow($this->produtos->get($id));
    	if(!$row) return $this->_redirect('lajes');
    	if($alias!=$row->alias) return $this->_redirect('lajes');
    	if($row->status_id!=1) return $this->_redirect('lajes');
    	$this->view->row = $row;
    	// _d($row);

    	$fotos = $this->produtos->q(
    		'select f.*, pf.produto_id, pf.foto_id from produtos_fotos pf '.
    		'left join fotos f on f.id = pf.foto_id '.
    		'where produto_id = '.$id
    	);
    	$this->view->fotos = $fotos;
    	// _d($fotos);

    	$fotos2 = $this->produtos->q(
    		'select f.*, pf.produto_id, pf.foto_id from produtos_fotos2 pf '.
            'left join fotos f on f.id = pf.foto_id '.
            'where produto_id = '.$id
        );
        $this->view->fotos2 = $fotos2;
        // _d($fotos2);

        $arqs = $this->produtos->q(
            'select a.*, pa.produto_id from produtos_arquivos pa '.
    		'left join arquivos a on a.id = pa.arquivo_id '.
    		'where produto_id = '.$id
        );
        $this->view->arqs = $arqs;
        // _d($arqs);

    	$categoria = $this->categorias->getWithProdutos($row->categoria_id,4);
    	$this->view->categoria = $categoria;
    	// _d($categoria);

    	$this->view->titulo = $row->titulo;
    	$this->view->meta_description = $row->descricao;
    	$this->view->meta_keywords = Is_Str::text2keywords($row->titulo.', '.$row->descricao.', '.$row->descricao_completa);
    	$this->view->meta_og_image = ($fotos) ? IMG_URL.'/produtos/'.$fotos[0]->path : null;
    }


}

