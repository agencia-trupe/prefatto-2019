<?php

class SuporteController extends Zend_Controller_Action
{

    public function init()
    {
        $this->table = new Application_Model_Db_Faqs();
    }

    public function indexAction()
    {
        $faqs = _utfRows($this->table->fetchAll(
        	null,
        	'ordem'
        ));
        $this->view->faqs = $faqs;
        // _d($faqs);

        $prods = $this->table->s(
        	'produtos',
        	'id,titulo,path,arquivo_nome',
        	'status_id=1 and path is not null and trim(path) not in ("",0)'
        );
        $this->view->produtos = $prods;
        // _d($prods);

        $arqs = $this->table->q(
            'select a.*, pa.produto_id, p.titulo from produtos_arquivos pa '.
            'left join arquivos a on a.id = pa.arquivo_id '.
            'left join produtos p on p.id = pa.produto_id '.
            // 'where produto_id = '.$id
            'order by produto_id, a.descricao '
        );
        $this->view->arqs = $arqs;
        // _d($arqs);
    }


}

