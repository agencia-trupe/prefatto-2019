<?php

class ServicosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->servicos = new Application_Model_Db_Servicos();
        $this->view->titulo = 'Serviços';
    }

    public function indexAction()
    {
        $rows = $this->servicos->fetchAllWithPhoto(
        	'status_id=1',
        	'ordem'
        );
        $this->view->rows = $rows;
        // _d($rows);
    }


}

