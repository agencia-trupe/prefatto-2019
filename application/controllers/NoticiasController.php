<?php

class NoticiasController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->noticias = new Application_Model_Db_Noticias();
        $this->table = $this->noticias;
        $this->view->titulo = 'Notícias';
    }

    public function indexAction($limit=null,$offset=null)
    {
        $noticias = $this->noticias->fetchAllWithPhoto(
        	'status_id = 1',
        	'id desc',
        	$limit,$offset,
        	array('group'=>'t1.id')
        );
        $this->view->noticias = $noticias;
        // _d($noticias);
    }

    public function noticiaAction()
    {
        $alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
        if(!$alias) return $this->_redirect('noticias');
        $alias = explode('-', $alias);
        $id = array_pop($alias);
        $alias = implode('-', $alias);
        // _d(array($id,$alias));
        
        $noticia = _utfRow($this->table->get($id));
        // _d($noticia);

        if(!$noticia || @$noticia->status_id == 0 || @$noticia->alias!=$alias){
            return $this->_forward('not-found','error','default',array('url'=>URL.'/blog'));
        }

        $this->indexAction(3);
        $this->view->noticias = $this->table->parseUrls($this->view->noticias);
        // _d($this->view->noticias);
        
        $this->view->noticia = $noticia;

        // meta tags
        // $this->view->titulo = $noticia->titulo;
        $this->view->meta_description = $noticia->titulo.': '.Php_Html::toText($noticia->body);
        // $this->view->meta_canonical = URL.'/noticia/'.$noticia->alias.'-'.$noticia->id;
        $this->view->meta_canonical.= '/'.@$noticia->categoria->alias.'/'.$noticia->alias.'-'.$noticia->id;
    }


}

