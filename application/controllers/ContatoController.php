<?php

class ContatoController extends ZendPlugin_Controller_Ajax
{
    protected $mensagem_id1 = 11;
    protected $mensagem_id2 = 12;

    public function requires()
    {
        $this->mailling = new Application_Model_Db_Mailling();
        $this->mensagens = new Application_Model_Db_Mensagens();
        $this->mensagens_contato = new Application_Model_Db_MensagensContato();
        $this->arquivos = new Application_Model_Db_Arquivos();
        
        $this->mensagem1 = $this->mensagens->get($this->mensagem_id1);
        $this->mensagem2 = $this->mensagens->get($this->mensagem_id2);
    }

    public function init()
    {
        /* Initialize action controller here */
        $this->view->meta_description = 'Contato '.SITE_TITLE;
        $this->messenger = new Helper_Messenger();

        $this->view->section = $this->section = "contato";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/".$this->section;
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/..".IMG_PATH."/".$this->section;
        // $this->file_path = $this->view->file_path = APPLICATION_PATH."/..".FILE_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;
        // _d($this->file_path);
    }

    public function indexAction()
    {
        $this->view->titulo = 'Contato';
    }
    
    public function enviarAction()
    {
        $this->requires();
        $r = $this->getRequest();
        if(!$r->isPost()) return $this->postMessage('Requisição inválida','error');
        
        $this->mensagem = $this->mensagem1;
        $post = $r->getPost();
        $post['mensagem_id'] = $this->mensagem_id1;
        $assunto = (bool)trim($r->getParam('assunto')) ? trim($r->getParam('assunto')) : $this->mensagem1->subject;
        $form = new Application_Form_Contato();
        $email_assunto = null;
        $post_mensagem_id = $post['mensagem_id'];

        if($form->isValid($post)){ // valida post
            // salvar em mensagens_contato
            $html = "<h1>Contato ".$assunto."</h1>". // monta html
                // "<b>Assunto:</b> ".$assunto."<br/><br/>".
                nl2br($r->getParam('mensagem'))."<br/><br/>".
                "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                "<b>E-mail:</b> <a href='mailto:".
                $r->getParam('email')."'>".$r->getParam('email').
                "</a><br/>".
                "<b>Telefone:</b> ".$r->getParam('telefone')."<br/>";
                // "<b>Empresa:</b> ".$r->getParam('empresa');
            
            $data_mensagem_contato = array(
                'nome'     => $post['nome'],
                // 'empresa'  => $post['empresa'],
                'email'    => $post['email'],
                'telefone' => Is_Cpf::clean($post['telefone']),
                // 'cpf'      => Is_Cpf::clean($post['cpf']),
                'assunto'  => $assunto,
                'mensagem' => $post['mensagem'],
                'data_cad' => date('Y-m-d H:i:s'),
                'mensagem_id' => $post['mensagem_id'],
            );

            try {
                // salva mensagem de contato
                $this->mensagens_contato->insert($data_mensagem_contato);

                // salvar em mailling (insertUpdate on duplicate)
                // $this->mailling->insertUpdate(array(
                //     'nome' => $post['nome'],
                //     'email' => $post['email'],
                //     'telefone' => Is_Cpf::clean($post['telefone']),
                //     'data_cad' => date('Y-m-d H:i:s'),
                // ));
                
                if(1 || ENV_PRO) try { // tenta enviar emails de contato
                    Trupe_Prefatto_Mail::sendWithReply( // email de contato
                        $post['email'],
                        $post['nome'],
                        'Contato '.$assunto,
                        $html,null,null,array(
                            'to' => $this->mensagem->email
                        )
                    );

                    Trupe_Prefatto_Mail::send( // resposta auto p/ cliente
                        $post['email'],
                        $post['nome'],
                        $this->mensagem->subject,
                        $this->mensagem->body
                    );
                } catch(Exception $e){
                    // pass mail error
                }
                
                return $this->postMessage('Mensagem enviada com sucesso!','msg');
            } catch(Zend_Form_Exception $e){
                return $this->postMessage($form->getMessage(', ',true),'error');
            } catch(Exception $e){
                return $this->postMessage('**'.$e->getMessage(),'error');
            }
        }
        
        return $this->postMessage('* Preencha todos os campos <br>'.$form->getMessage(', ',true),'error');
    }

    public function orcamentoAction()
    {
        $id = ($this->_hasParam('id')) ? $this->_getParam('id') : null;
        $tipo = ($this->_hasParam('tipo')) ? $this->_getParam('tipo') : null;
        $produto = ($this->_hasParam('produto')) ? $this->_getParam('produto') : null;
        if($produto) $this->view->produto_id = $produto;
        // _d($id,0); _d($tipo,0); _d($produto);

        $this->paginas = db_table('paginas');
        $pagina = $this->paginas->getPagina(5);
        $this->view->pagina = $pagina;
        // _d($pagina);

        $this->getProKV();
        // _d($this->proKV);
    }

    public function getProKV()
    {
        $this->produtos = db_table('produtos');
        $produtos = $this->produtos->s(
            'produtos',
            'id,titulo',
            'status_id=1',
            'titulo'
        );
        $this->view->produtos = $produtos;
        $proKV = array(''=>'tipo de laje [selecione...]');
        foreach ($produtos as $pro) $proKV[$pro->id] = $pro->titulo;
        $this->view->proKV = $proKV;
        $this->proKV = $proKV;
        return $proKV;
    }

    public function orcamentoEnviarAction()
    {
        $this->requires();
        $this->getProKV();
        $r = $this->getRequest();
        $url = 'contato/orcamento';
        if(!$r->isPost()) return $this->postMessage('Requisição inválida','error',$url);
        $file = null; $rename = null;
        
        $this->mensagem = $this->mensagem2;
        $post = $r->getPost();
        $post['mensagem_id'] = $this->mensagem->id;
        $assunto = (bool)trim($r->getParam('assunto')) ? trim($r->getParam('assunto')) : $this->mensagem->subject;
        $form = new Application_Form_Contato();
        $email_assunto = null;
        $post_mensagem_id = $post['mensagem_id'];

        if($form->isValid($post)){ // valida post
            // salvar em mensagens_contato
            // upload do arquivo
            if((bool)@$_FILES['arquivo']) if((bool)@$_FILES['arquivo']['size']){
                $file = $_FILES['arquivo'];

                $v = array( // validações
                    'ext' => 'doc,docx,xls,xlsx,pdf,odt,rtf,dwg,dxf'.
                            ',jpg,jpeg,png,gif,bmp,tiff',
                    'size' => '10mb'
                );

                $filename = $file['name'];
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Extension', false, $v['ext'])
                       ->addValidator('Size', false, array('max' => $v['size']))
                       ->addValidator('Count', false, 1)
                       ->addFilter('Rename',$this->file_path.'/'.$rename)
                       ->setDestination($this->file_path);
                
                if(!$upload->isValid()){
                    return $this->postMessage('O arquivo deve possuir até '.$v['size'].'<br/> e ter uma das extensões a seguir: '.$v['ext'].'.','error',$url);
                }
            }


            $html = "<h1>Contato ".$assunto."</h1>". // monta html
                // "<b>Assunto:</b> ".$assunto."<br/>".
                nl2br($r->getParam('mensagem'))."<br/><br/>".
                "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                "<b>E-mail:</b> <a href='mailto:".
                $r->getParam('email')."'>".$r->getParam('email').
                "</a><br/>".
                "<b>Local:</b> ".$r->getParam('local')."<br/>".
                // "<b>Fase:</b> ".$this->faseKV[$post['fase']]."<br/>".
                "<b>Tipo:</b> ".$this->proKV[$post['tipo']]."<br/>".
                "<b>Telefone:</b> ".$r->getParam('telefone')."<br/>";
            
            $data_mensagem_contato = array(
                'nome'     => $post['nome'],
                // 'cpf'      => Is_Cpf::clean($post['cpf']),
                'telefone' => Is_Cpf::clean($post['telefone']),
                'email'    => $post['email'],
                'cidade'   => $post['local'],
                'assunto'  => $assunto,
                'mensagem' => $post['mensagem'],
                'data_cad' => date('Y-m-d H:i:s'),
                'mensagem_id' => $post['mensagem_id'],
                'fase_id'  => $post['fase'],
                'tema_id'  => $post['tipo'],
                'tema'     => $this->proKV[$post['tipo']],
            );

            try {
                if($file){ // upload final e cadastro de arquivo
                    $upload->receive();

                    $data_mensagem_contato['arquivo_id'] = $this->arquivos->insert(array(
                        "descricao" => $filename,
                        "path"     => $rename,
                        "data_cad" => date("Y-m-d H:i:s")
                    ));

                    $c_url = FILE_URL.'/'.$this->section.'/'.$rename;

                    $html.= '<br/><br/><a href="'.$c_url.'">Visualizar arquivo</a><br/>';
                }

                // salva mensagem de contato
                $this->mensagens_contato->insert($data_mensagem_contato);

                // salvar em mailling (insertUpdate on duplicate)
                // $this->mailling->insertUpdate(array(
                //     'nome' => $post['nome'],
                //     'email' => $post['email'],
                //     'telefone' => Is_Cpf::clean($post['telefone']),
                //     'data_cad' => date('Y-m-d H:i:s'),
                // ));
                
                if(1 || ENV_PRO) try { // tenta enviar emails de contato
                    Trupe_Prefatto_Mail::sendWithReply( // email de contato
                        $post['email'],
                        $post['nome'],
                        'Contato '.$assunto,
                        $html,null,null,array(
                            'to' => $this->mensagem->email
                        )
                    );

                    Trupe_Prefatto_Mail::send( // resposta auto p/ cliente
                        $post['email'],
                        $post['nome'],
                        $this->mensagem->subject,
                        $this->mensagem->body
                    );
                } catch(Exception $e){
                    // pass mail error
                }
                
                return $this->postMessage('Mensagem enviada com sucesso!','msg',$url);
            } catch(Zend_Form_Exception $e){
                return $this->postMessage($form->getMessage(', ',true),'error',$url);
            } catch(Exception $e){
                return $this->postMessage('**'.$e->getMessage(),'error',$url);
            }
        }
        
        return $this->postMessage('* Preencha todos os campos <br>'.$form->getMessage(', ',true),'error',$url);
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }


}