<?php

class QuemSomosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
        $this->diferenciais = new Application_Model_Db_Diferenciais();
        $this->clientes = new Application_Model_Db_Clientes2();
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina(2);
        $this->view->pagina = $pagina;
        // _d($pagina);
        
        $pagina2 = $this->paginas->getPagina(3);
        $this->view->pagina2 = $pagina2;
        // _d($pagina2);
        
        $pagina3 = $this->paginas->getPagina(4);
        $this->view->pagina3 = $pagina3;
        // _d($pagina3);
        
        $diferenciais = _utfRows($this->diferenciais->fetchAll(
        	'status_id=1',
        	'ordem'
        ));
        $this->view->diferenciais = $diferenciais;
        // _d($diferenciais);
        
        $clientes = $this->clientes->fetchAllWithPhoto(
        	'status_id=1',
        	'ordem'
        );
        $this->view->clientes = $clientes;
        // _d($clientes);
    }


}

