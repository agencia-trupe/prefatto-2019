<?php

class Admin_Form_Faqs extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/faqs/save')
             ->setAttrib('id','frm-faqs')
             ->setAttrib('name','frm-faqs');
        
        // elementos
        // $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('text','link',array('label'=>'Link do site','class'=>'txt'));
        // $this->addElement('hidden','alias');
        // $this->addElement('text','autor',array('label'=>'Autor','class'=>'txt'));
        // $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        $this->addElement('text','pergunta',array('label'=>'Pergunta','class'=>'txt'));
        $this->addElement('textarea','resposta',array('label'=>'Resposta','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('resposta')->setAttrib('rows',5)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('titulo')->setRequired();
        $this->getElement('pergunta')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

