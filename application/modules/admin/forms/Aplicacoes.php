<?php

class Admin_Form_Aplicacoes extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post')->setAction('')->setAttrib('id','frm-aplicacoes')->setAttrib('name','frm-aplicacoes');
		
		$this->addElement('text','descricao',array('label'=>'Nome:','class'=>'txt'));
		$this->addElement('text','body',array('label'=>'Descrição:','class'=>'txt'));
        $this->getElement('descricao')->setRequired();
        
		//$this->addElement('text','alias',array('label'=>'Alias:','class'=>'txt alias disabled','disabled'=>true));
        
        $this->addElement('submit','submit',array('label'=>'Enviar','class'=>'bt bt-left'));
        
        $this->removeDecs(array('label','htmlTag','description','errors'));
    }
    
    public function removeDecs($decorators = array('label','htmlTag','description','errors'),$elms=array())
    {
        $_elms = &$this->getElements();
        $elms = count($elms) ? $elms : $_elms;
        foreach($elms as $elm){
            foreach($decorators as $decorator){
                $elm->removeDecorator($decorator);
            }
        }
        return $this;
    }
}