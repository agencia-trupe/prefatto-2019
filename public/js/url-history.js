var hasPush = supports_history_api(), isPushed = false;

if(_ic_curso) {
	window.setTimeout(function(){
		// _show('#curso-'+_ic_curso);
		$('#curso-'+_ic_curso).trigger('click');
	},(APPLICATION_ENV=='development' ? 1000 : 0));
}

var filters = $('.accordion-cursosinc'),
	isFiltered = false,
	hash = '#', hash1 = '';

filters.click(function(e){
	e.preventDefault();

	var $this = $(this),
		id = $this.data('id'),
		hash = '#'+id,
		url = id.replace('curso-','curso/');

    // var $this = $(this);
	// hash1= $this.attr('href').split('/').reverse().shift();
	hash1 = [URL,CONTROLLER,url].join('/');
	// hash = '#main-index-'+(hash1.length ? hash1 : 'banner');
	// console.log([url,hash,hash1]); return false;
	
	// filters.removeClass('active');
	// $this.addClass('active');

	_show(hash);

	var filterType = 'cursoinc', psData = {};
		psData[filterType] = hash;
	_push(psData,filterType,hash1);
	// console.log(['send ga pageview', hasGA, hash1]);
    if(hasGA) ga('send', 'pageview', hash1);
    
	// filters.each(function(i,elm){
	// 	if(elm.href == $this.attr('href'))
	// 		$(elm).addClass('active');
	// });
	
	return false;
});

// filtro inicial
/*if(location.href != URL+'/'){
	var filter = location.href.split('/').reverse().shift();

	// _show('#main-index-'+filter);
	// filters.filter('.empresa-'+filter).addClass('active');
	window.setTimeout(function(){
		// _show('#main-index-'+filter);
	},(APPLICATION_ENV=='development' ? 1000 : 500));
	
	// var elm = document.getElementById('main-index-'+filter);
	// elm.scrollIntoView();
	// $(window).scrollTop(elm.offset().top).scrollLeft(elm.offset().left);
}*/

function _show(hash){
	_loadScrollTo(function(){
		var scrollOffsetTop = 0;//isSmartphone ? -40 : -40;
		window.setTimeout(function(){
			$.scrollTo($(hash),500,{
				axis:'y'
				// ,margin:true
				,offset: {top:scrollOffsetTop,left:0}
				// ,over:-0.5
			});
		},(APPLICATION_ENV=='development' ? 1000 : 500));

		// if(isSmartphone) $('#top-menu').removeClass('show');
	});
}

function supports_history_api() {
	return !!(window.history && history.pushState);
}

function _push(data,name,url){
	if(hasPush){
		history.pushState(data,name,url);
		isPushed = true;
	}
}

// marcando menu ao scroll
/*var mains_index = [], lastScrolled = '';
$('.main.main-index').each(function(i,elm){
	mains_index.push('#'+elm.id);
});
$(document).scroll(function(e){
	for(var mii in mains_index)
	if(isElementInView(mains_index[mii]))
	if(mains_index[mii]!=lastScrolled) {
		filters.removeClass('active');
		filters.eq(mii).addClass('active');
		lastScrolled = mains_index[mii];
		// console.log([mii,mains_index[mii]]);
	}
});

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function isElementInView(element, fullyInView) {
    var pageTop = $(window).scrollTop();
    var pageBottom = pageTop + $(window).height();
    var elementTop = $(element).offset().top;
    var elementBottom = elementTop + $(element).height();
    
    // fix for show only when a half or more is visible
    var scrollPadding = $(window).height()/2;
    elementTop+= scrollPadding;
    elementBottom+= scrollPadding;

    return (fullyInView === true) ?
        ((pageTop < elementTop) && (pageBottom > elementBottom)) :
    	((elementTop <= pageBottom) && (elementBottom >= pageTop));
}*/

/*var hasPush = supports_history_api(), isPushed = false;

var filters = $('.filter > ul > li > a'),
	pages = $('.empresa-page');

filters.click(function(e){
	var $this = $(this),
		filter = $this.attr('href').split('#')[1].replace('empresa-','');

	filters.removeClass('active');
	$this.addClass('active');

	_show($this.attr('href'));

	var filterType = 'page', psData = {};
		psData[filterType] = filter;
	_push(psData,filterType,'/empresa/'+filter);
});

// filtro inicial
if(location.href != URL+'/'+CONTROLLER){
	var params = location.href.replace(URL+'/'+CONTROLLER+'/','').split('/'),
		filter = params[0];

	_show('#empresa-'+filter);
	filters.filter('.empresa-'+filter).addClass('active');
}

function _show(elm){
	pages.removeClass('show');
	$(elm).addClass('show');
}

function supports_history_api() {
	return !!(window.history && history.pushState);
}

function _push(data,name,url){
	if(hasPush){
		history.pushState(data,name,url);
		isPushed = true;
	}
}*/