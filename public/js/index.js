if($('#slideshow .foto').size()>1) _loadCycle(function(){
	$('#slideshow').before('<ul id="banner-nav">').cycle({
		slideResize: true,
		containerResize: true,
		width: '100%',
		height: '100%',
		fit: 1,
		
		fx:'fade',
		speed:2000,
		// timeout: 0,
		// speedIn:4000,
		// speedOut:100,
		pager:"#banner-nav"
	});
});

if($('#depoimentos .depoimento').size()>1) _loadCycle(function(){
	$('#depoimentos-list').cycle({
        fx:      'scrollHorz',
        prev:    '#dep-prev',
        next:    '#dep-next',
        // timeout:  0,
        timeout: 7000,
        speed: 1200,
        manualSpeed: 1200
    });
});
