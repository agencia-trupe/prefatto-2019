// console.log('suporte');

var $formdl = $('#frm-download'), 
	$file = $('#file'), 
	$produtoid = $('#produto_id');
// $formdl.submit(function(e){ // download por redirecionamento
// 	if($file.val()=='') {
// 		e.preventDefault();
// 		alert('Selecione um produto');
// 		return false;
// 	}

// 	this.action = FILE_URL+'/produtos/'+$file.val();
// 	return true;
// });
$formdl.submit(function(e){ // download com trigger click no link
	e.preventDefault();
	
	if($file.val()=='') {
		alert('Selecione um produto');
		return false;
	}

	document.getElementById('file-'+$produtoid.val()).click();
	return false;
});
$produtoid.change(function(e){
	// $file.val($('option:selected',this).data('file'));

    if(this.value==''){
        $('#downloads ul li').show();
        return;
    }
    $('#downloads ul li').hide().filter('.li-fp-'+this.value).show();
});
$('.submit',$formdl).click(function(e){
	$formdl.submit();
});


var Accordion = {
    wrapper: $('.accordion-wrapper'),

    open: function(elm){
        elm.addClass('open').addClass('active');
        $(elm.attr('href')).slideDown();
    },

    close: function(elm){
        elm.removeClass('open').removeClass('active');
        $(elm.attr('href')).slideUp();
    },

    closeAll: function(){
        if(this.wrapper.hasClass('accordion-keep-opens')) return;
        $('.accordion-item',this.wrapper).removeClass('open').removeClass('active');
        $('.accordion-content',this.wrapper).slideUp();
    },

    init: function(){
        var _acc = this;
        
        this.wrapper.find('.accordion-item').each(function(i,elm){
            var $elm = $(elm);
            $elm.click(function(e){
                e.preventDefault();
                
                if($elm.hasClass('open')||$elm.hasClass('active'))
                    return _acc.close($elm);

                _acc.closeAll();
                _acc.open($elm);
                // _sto(function(){ _acc.open($elm); },200);
            });
        });
    }
}


if(Accordion.wrapper.size()) Accordion.init();




