console.log('contato');

$(document).ready(function(){
    _prevalues();
    
    $('input#telefone').focus(function(){
        $(this).removeClass('pre');
    }).blur(function(){
        var $t = $(this);
        if($t.val() == ''){
            $t.addClass('pre');
        }
    });
    
    // enviando contato
    var $formContato = $('form.contato');
    $('a#submit',$formContato).click(function(e){
        $formContato.submit();
    });
    $formContato.submit(function(e){
        if($formContato.hasClass('send')) return true;
        e.preventDefault();
        var $status = $('.status',$formContato).removeClass('error');
        $status.html('Enviando...');
        
        head.js(JS_PATH+'/is.simple.validate.js',function(){
            if($formContato.isSimpleValidate({pre:true})){
                var action = (ACTION=='index'?'':ACTION+'-')+'enviar',
                    url  = URL+'/'+CONTROLLER+'/'+action+'.json',
                    data = $('form.contato').serialize();
                
                // console.log(url); return;
                if(ACTION=='orcamento') {
                    $formContato.addClass('send').trigger('submit');
                    return true;
                }

                $.post(url,data,function(json){
                    if(json.error){
                        $status.addClass('error').html(json.error);
                        return false;
                    }
                    
                    // $status.html(json.msg);
                    $status.html(json.msg);
                    _resetForm('form.contato');
                    /*Shop.messageBox('<p class="shop-message-title contato">'+json.msg+'</p>'+
                                    '<p class="shop-message-buttons">'+
                                    '<a href="#" class="shop-close-message-box fechar-contato single">Fechar</a>'+
                                    '</p>');*/
                },'json');
            } else {
                $status.addClass('error').html('* Preencha todos os campos');
            }
        });
        return false;
    });

    // orcamento
    if(ACTION=='orcamento'){
    $.getCss(JS_PATH+'/fileinput/fileinput.css');
    head.js(JS_PATH+'/fileinput/jquery-ui-1.8.9.custom.min.js',JS_PATH+'/fileinput/jquery.fileinput.min.js',function(){
        $('#arquivo').fileinput({
            // inputText: 'ANEXAR CURRÍCULO',
            buttonText: 'ANEXAR PROJETO '+(isMobile ? '' : '(dwg | dxf | pdf | jpg | word | excel)')
            // buttonText: 'SELECIONAR'
        });
        $('#arquivo').change(function(e){
            if($(this).val().length)
                $('.fileinput-wrapper').addClass('has-file');
            else
                $('.fileinput-wrapper').removeClass('has-file');
        });
    });
    } // /if ACTION orcamento
});

function _prevalues(v){
    $('.prevalue').each(function(){
        var $t = $(this);
        $t.prevalue(v || $t.data('prevalue'));
    });
}

function _resetForm(formSelector){
    $(formSelector).find('input,textarea').val('');
    $(formSelector).find('select option:first-child').attr('selected',true);
}


function _test() {
    $('#nome').val('TESTE');
    $('#email').val('contato'+SITE_NAME+'@mailinator.com');
    $('#telefone').val('(11)99999-9999');
    $('#local').val('TESTE');
    $('#fase').val('');
    $('#tipo').val('1');
    $('#mensagem').val('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, ad. Voluptatum veritatis delectus vero placeat perspiciatis maxime omnis, nesciunt, expedita excepturi earum eveniet quibusdam. Quo possimus magni nisi laboriosam cupiditate.');
    // $('#arquivo').val('');
}
if(ENV_DEV) _test();
