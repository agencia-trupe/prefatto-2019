/*global $ */
var $table, _id = 1, _tableId;

$(document).ready(function () {
	// $table = $('#tabela-de-medidas table.table-medidas');
	// _id = Number($table.find('tbody tr:first-child td:last-child input.medidas-id').val())-1;
	showHideDelButtonTableMedidas();

	$('#tabela-de-medidas .tools .add').click(function(e){
		_tableId = $(this).data('tabela-id');
		$table = $('#tabela_medida_'+_tableId);
		_id = Number($table.find('tbody tr:first-child td:last-child input.medidas-id').val())-1;
		_id = _id + $table.find('.del').length;//1;
		
		var newTable = {
				id : _id,
				th : '<td>'+
					'<input type="text" class="txt" name="medidas[tamanho]['+_tableId+'][]" id="medidas_tamanho_'+this.id+'" value="" />'+
					'</td>',
				tf : '<td>'+
					'<a href="#" class="tabela-column-del del" data-tabela-id="'+_tableId+'">Excluir</a>'+
					'</td>',
				td : function(f) {
					return '<td><input type="text" class="txt" name="medidas['+f+']['+_tableId+'][]" id="medidas_'+f+'_'+this.id+'" value="" />'+
						   '<input type="hidden" name="medidas[id]['+_tableId+']['+this.id+']" class="medidas-id" id="medidas_id_'+this.id+'" value="" /></td>';
				}
			};

		$table.find('thead tr').append(newTable.th);
		
		$table.find('tbody tr').each(function(i,elm){
			var $this = $(this);
			$this.append(newTable.td($this.data('campo')));
		});

		$table.find('tfoot tr').append(newTable.tf);
		showHideDelButtonTableMedidas();
		$table.find('thead tr td:last-child input').focus();
	});

	$('#tabela-de-medidas .del').live('click',function(e){
		$table = $('#tabela_medida_'+$(this).data('tabela-id'));
		var $this = $(this),
			index = $this.parent().index() + 1,
			rows  = [
				'thead tr td:nth-of-type('+index+')',
				'tbody tr td:nth-of-type('+index+')',
				'tfoot tr td:nth-of-type('+index+')'
			];

		if(confirm('Deseja remover o tamanho selecionado?'))
			$table.find(rows.join(',')).remove();

		showHideDelButtonTableMedidas();
	});
    
});

function showHideDelButtonTableMedidas(){
	$('.table-medidas').each(function(i,elm){
		var dels = $(elm).find('.del');
		dels.hide();
		if(dels.length>1)  dels.show();
	});
}