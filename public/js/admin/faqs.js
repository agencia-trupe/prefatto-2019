var MAX_FOTOS = 1, MAX_SIZE = 5242880, allow_photos = false;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();
    _contentImgSize();        
    
    $("#frm-"+DIR).submit(function(e){
        // if($.trim($("#titulo").val()).length == 0){
        if($.trim($("#pergunta").val()).length == 0){
            $("#titulo").focus();
            Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
            return false;
        }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });

    // $('#categorias .row-list li.title').draggable({revert:'invalid'});
    // $('#categorias .row-list li.title').disableSelection();

    var $checksCat = $('input.check-cat'), checks_cat = [],
        $catId = $('#categorias_id'), $check;
    $checksCat.change(function(e){
        checks_cat = [];
        $catId.val('');

        $checksCat.each(function(e){
            if(this.checked) checks_cat.push(this.value);
        });
        
        if(checks_cat.length) $catId.val(','+checks_cat.join(',')+',');
    });
});
