$(document).ready(function(){
    // $("#video-data").prevalue("Data");
    // $("#video-valor").prevalue("Valor");
    
    // envio do formulário
    $("#videos-submit").click(function(e){
        e.preventDefault();
        var video_url = $("#video-url").val().length > 0 ? $("#video-url").val() : false;
        
        // if(sugestao_id){
            var data = "obra_id="+$("#id").val()+
                       "&url="+video_url;
            if(!video_url) return alert('URL inválida');
            
            $.getJSON(URL+"/admin/obras/videos-add.json?"+data,function(json){
                if(!json.error){
                    var tmpl_data = {
                        id:json.id,
                        url:$("#video-url").val(),
                        url_short:''
                    };
                    tmpl_data.url_short = tmpl_data.url
                        .replace('http://','')
                        .replace('https://','')
                        .replace('www.','');
                    $("#tmplVideos").tmpl(tmpl_data).appendTo(".videos-list");
                    $("#video-url").val("").blur();
                } else {
                    $("#video-url").val("").blur();
                    alert(json.error);
                }
            });
        // }
    });
    
    $(".videos-delete").live('click',function(){
        if(confirm('Deseja remover o video?')){
            var $row = $(this).parent(),
                data = "obra_id="+$("#id").val()+"&video_id="+$row.find(".videos-id").val();
            $.getJSON(URL+"/admin/obras/videos-del.json?"+data,function(json){
                if(!json.error){
                    $row.fadeOut("fast",function(){
                        $row.remove();
                    });
                    $("#video").val(json.video_total);
                } else {
                    alert(json.error);
                }
            });
        }
    });
});